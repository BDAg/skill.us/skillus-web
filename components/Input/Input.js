import React from "react";
import style from "./Input.module.css";

const Input = ({
  mask,
  icon,
  placeholder,
  toggleRequiredField,
  required,
  type,
  value,
  setValue,
  specialValidation,
  message,
}) => {
  return (
    <>
      <div className={type === "textArea" ? style.backgroundInput : style.backgroundTextArea} style={{height: type === "textArea" ? 144 : 48}}>
        <i className={icon} style={{ padding: "0 12px" }}/>
        {type === "textArea" ?
          <textarea
            onBlur={required ? toggleRequiredField : null}
            value={value}
            onChange={(e) => (mask ? setValue(mask(e.target.value)) : setValue(e.target.value))}
            className={style.textArea}
            type={type}
            id={placeholder}
            required
            rows="4"
          />:
          <input
            onBlur={required ? toggleRequiredField : null}
            value={value}
            onChange={(e) => (mask ? setValue(mask(e.target.value)) : setValue(e.target.value))}
            className={style.inputText}
            type={type}
            id={placeholder}
            required
          />
        }
        {specialValidation === false && <i className="fas fa-times" />}
        <span className={style.floatinglabel} style={{marginLeft: icon ? null : 16}}>{placeholder}</span>
      </div>
      <div
        className="errorMessage"
        id={`${placeholder}_em_`}
        style={{ fontSize: 12, color: "red", display: "none", position: "absolute" }}>
        {message || `${placeholder} deve ser preenchido`}
      </div>
      <div style={{ marginBottom: 24 }} />
    </>
  );
};

export default Input;
