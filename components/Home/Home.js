import React, { useContext } from "react";
import style from "./Home.module.css";
import Link from "next/link";
import { UserContext } from "../../context/userContext";

const Home = () => {
  const { user } = useContext(UserContext);
  return (
    <div className={style.containerImage}>
      <img className={style.backgroundImage} src="img/nozes.png" />
      <h1 className={style.textIntroduction}>
        <div className={style.homeTextTitle}>Bem vindo ao <br />Skill.us</div>
        <br />
        <div className={style.homeTextHelp}>Crie perguntas e discuta com a comunidade.</div>
        <br />
        <Link href={user ? "/problem" : "login"}>
          <span className={style.start}>
            <span style={{ color: "#993399" }}>Começe Agora!</span>
          </span>
        </Link>
      </h1>
    </div>
  );
};

export default Home;
