import React, {useState} from 'react'
import Input from '../Input/Input';
import Link from 'next/link';
import style from './About.module.css';
import { useMutation, useQuery } from '@apollo/react-hooks';
import { editUser } from '../../graphql/userAbout';
import { successToast, errorToast } from '../Toast/Toast';
import ReactLoading from 'react-loading';
import { useRouter } from "next/router";

const Edit = ({data}) => {
    const [ EditUser ] = useMutation(editUser);
    const [name, setName] = useState(data.user.name);
    const [description, setDescription] = useState(data.user.description);
    const [email, setEmail] = useState(data.user.email);
    const [cellphone, setCellphone] = useState(data.user.telephone);
    const [gitlab, setGitlab] = useState(data.user.contact[1].value);
    const [linkedin, setLinkedin] = useState(data.user.contact[0].value);
    const [photo, setPhoto] = useState(data.user.photo);
    const [ loadingSubmit, setLoadingSubmit ] = useState(false);
    const Router = useRouter();

    const handleSubmit = async (e) => {
        e.preventDefault();
        setLoadingSubmit(true);
        try {
            const result = await EditUser({
              variables: {
                    id: data.user.id,
                    name: name,
                    email: email,
                    telephone: cellphone,
                    description: description,
                    skill: null,
                    contact: [linkedin, gitlab],
                    photo: photo,
                    user_status_id: null
                },
            });
            console.log(result);
            if (!result.data.updateProfile) {
              errorToast(":chipmunk: Erro ao atualizar dados!");
            } else {
                successToast(":chipmunk: Dados atualizados com sucesso!");
                setTimeout(() => Router.replace(`/profile/${data.user.id}`), 2000);
            }
            } catch (error) {
                setLoadingSubmit(false);
                console.log(error)
                errorToast(":chipmunk: Network Error !");
        }
    };
    
    return(
        <form onSubmit={async (e) => await handleSubmit(e)}>
            <div className={style.containerPhotos}>
                <img onClick={() => setPhoto('https://i.imgur.com/0nJ6mOe.png')} style={{opacity: photo === 'https://i.imgur.com/0nJ6mOe.png' ? 1 : 0.4 }} src={'https://i.imgur.com/0nJ6mOe.png'}/>
                <img onClick={() => setPhoto('https://i.imgur.com/AbjupGx.png')} style={{opacity: photo === 'https://i.imgur.com/AbjupGx.png' ? 1 : 0.4 }} src={'https://i.imgur.com/AbjupGx.png'}/>
                <img onClick={() => setPhoto('https://i.imgur.com/cpPs0UN.png')} style={{opacity: photo === 'https://i.imgur.com/cpPs0UN.png' ? 1 : 0.4 }} src={'https://i.imgur.com/cpPs0UN.png'}/>
                <img onClick={() => setPhoto('https://i.imgur.com/Euhyyyp.png')} style={{opacity: photo === 'https://i.imgur.com/Euhyyyp.png' ? 1 : 0.4 }} src={'https://i.imgur.com/Euhyyyp.png'}/>
                <img onClick={() => setPhoto('https://i.imgur.com/NS1PgdI.png')} style={{opacity: photo === 'https://i.imgur.com/NS1PgdI.png' ? 1 : 0.4 }}src={'https://i.imgur.com/NS1PgdI.png'}/>
            </div>
            <div>
                <Input
                    placeholder={"Nome"}
                    icon={false}
                    type="text"
                    value={name}
                    setValue={setName}
                />
                <Input
                    placeholder="Sobre você"
                    type="textArea"
                    value={description}
                    setValue={setDescription}
                    icon={false}
                />
                <Input
                    placeholder={"Email"}
                    icon={false}
                    type="text"
                    value={email}
                    setValue={setEmail}
                />
                <Input
                    placeholder={"Celular"}
                    icon={false}
                    type="text"
                    value={cellphone}
                    setValue={setCellphone}
                />
                <Input
                    placeholder={"Gitlab"}
                    icon={false}
                    type="text"
                    value={gitlab}
                    setValue={setGitlab}
                />
                <Input
                    placeholder={"Linkedin"}
                    icon={false}
                    type="text"
                    value={linkedin}
                    setValue={setLinkedin}
                />
            </div>
            {loadingSubmit ? (
                <div style={{ marginTop: 10, justifyContent: 'flex-end', display: 'flex'}}>
                    <div style={{ marginRight: 10 }}>
                        <ReactLoading type={'spin'} color={'#f77f5c'} height={30} width={30} />
                    </div>
                    <Link href={'/problem'}>
                        <div className="borderButton" style={{ marginRight: 10 }}> Cancelar </div>
                    </Link>
                    <div className="disabledButton" type="submit">
                        Enviar
                    </div>
                </div>
            ) : (
                <div style={{ marginTop: 10, justifyContent: 'flex-end', display: 'flex'}}>
                    <Link href={'/problem'}>
                        <div className="borderButton" style={{ marginRight: 10 }}> Cancelar </div>
                    </Link>
                    <button className="confirmButton" type="submit">
                        Enviar
                    </button>
                </div>
            )}
        </form>
    )
}

export default Edit;
