import React, { useState } from "react";
import style from "./About.module.css";
import { withApollo } from "../../lib/apollo";
import { useQuery } from "@apollo/react-hooks";
import ListProblem from "../Problem/ListProblem";
import ListProblemCard from "../Problem/ListProblemCard";
import { problemByUser } from "../../graphql/problem";
import { rating } from "../../graphql/rating";

const About = ({ userData }) => {
  const [profile, setProfile] = useState("all");
  const { loading, data } = useQuery(problemByUser, {
    variables: { userId: userData.user.id },
  });
  const { loading: loadingR, data: dataR } = useQuery(rating, {
    variables: { userId: userData.user.id },
  });

  if (loading || loadingR) {
    return null;
  }

  return (
    <div className={`${style.container} flex column`}>
      <div className="full">
        <div className={`${style.secondContainer} cardSimple shadow flex column center`}>
          <div className="flex row">
            <div className={style.imageDiv}>
              <img src={userData.user.photo} alt="profile" className={style.img} />
            </div>
          </div>
          <h4>{userData.user.name}</h4>
          <p>
            <i className="fas fa-star starsColor " /> {userData.user.soma}
          </p>
          <p>{userData.user.description}</p>
        </div>
      </div>
      {profile === "all" ? (
        <div className={style.aboutUser}>
          <div className="cardSimple shadow flex column center primaryColor">
            <h3 style={{ marginBottom: 10 }}>Principais Habilidades</h3>
            <div className={style.skillList}>
              {userData.user.skill.map((skill, index) => (
                <span key={index}>{`${skill.name}`}</span>
              ))}
            </div>
          </div>
          <div className="flex full row">
            <div className="cardSimple full shadow center flex column">
              <h3 style={{ marginBottom: 10 }} className="primaryColor">
                Contato
              </h3>
              <div className="full flex column" style={{ paddingLeft: 110 }}>
                <a
                  target="_blank"
                  rel="noreferrer"
                  href={`https://web.whatsapp.com/send?phone=55${userData.user.telephone}`}
                  className="full pointer"
                  style={{ marginBottom: 10 }}>
                  <i className="fab fa-whatsapp primaryColor" /> {userData.user.telephone}
                </a>
                <a className="full pointer" style={{ marginBottom: 10 }}>
                  <i className="fas fa-envelope primaryColor" /> {userData.user.email}
                </a>
                <a
                  target="_blank"
                  rel="noreferrer"
                  href={userData.user.contact[1].value}
                  className="full pointer"
                  style={{ marginBottom: 10 }}>
                  <i className="fab fa-gitlab primaryColor" />{" "}
                  {userData.user.contact[1].value.split("/").slice(-1)[0]}
                </a>
                <a
                  target="_blank"
                  rel="noreferrer"
                  href={userData.user.contact[0].value}
                  className="full pointer"
                  style={{ marginBottom: 10 }}>
                  <i className="fab fa-linkedin-in primaryColor" /> {userData.user.name}
                </a>
              </div>
            </div>
            <div className="cardSimple shadow full flex column center pointer">
              <h3
                className={`${style.titleCard} primaryColor pointer`}
                onClick={() => setProfile("rates")}>
                Avaliações Recentes
              </h3>
              {dataR.ratings.slice(0, 3).map((rate, index) => (
                <div className="flex full" style={{ marginTop: 10 }} key={index}>
                  <img src={rate.evaluator.photo} alt="profile" className={style.problemPhoto} />
                  <div>
                    <div className="flex">
                      <p className={`${style.content} bold`}>{rate.evaluator.name}</p>
                    </div>
                    <p className={`${style.content} textPrimaryColor`}>{rate.comment}</p>
                  </div>
                  <p className={`${style.content} primaryColor bold left flex center`}>
                    <i className="fas fa-star starsColor " /> {rate.note}
                  </p>
                </div>
              ))}
            </div>
          </div>
          <div className="flex full row">
            <div className="cardSimple full shadow flex column center">
              <h3
                className={`${style.titleCard} primaryColor pointer`}
                onClick={() => setProfile("problemsResolved")}>
                Minhas colaborações
              </h3>
              <ListProblemCard
                problemList={data.problemsByUser.filter((problem) => {
                  return problem.helper?.id === userData?.user?.id;
                })}
                amount={3}
              />
            </div>
            <div className="cardSimple full shadow flex column center">
              <h3
                className={`${style.titleCard} primaryColor pointer`}
                onClick={() => setProfile("problemsOpened")}>
                Meus problemas
              </h3>
              <ListProblemCard
                problemList={data.problemsByUser.filter((problem) => {
                  return problem.creator?.id == userData?.user?.id;
                })}
                amount={3}
              />
            </div>
          </div>
        </div>
      ) : profile === "problemsResolved" ? (
        <div className="cardSimple">
          <div className="flex between primaryColor center" style={{ fontSize: 20, margin: 10 }}>
            <i onClick={() => setProfile("all")} className="fas fa-arrow-left pointer" />
            <h3 style={{ marginBottom: 10 }}>Minhas colaborações</h3>
            <div />
          </div>
          <ListProblem
            problemList={data.problemsByUser.filter((problem) => {
              return problem.helper?.id == userData?.user?.id;
            })}
          />
        </div>
      ) : profile === "problemsOpened" ? (
        <div className="cardSimple">
          <div className="flex between primaryColor center" style={{ fontSize: 20, margin: 10 }}>
            <i onClick={() => setProfile("all")} className="fas fa-arrow-left pointer" />
            <h3 style={{ marginBottom: 10 }}>Meus problemas</h3>
            <div />
          </div>
          <ListProblem
            problemList={data.problemsByUser.filter((problem) => {
              return problem.creator?.id == userData?.user?.id;
            })}
          />
        </div>
      ) : profile === "rates" ? (
        <div className="cardSimple">
          <div className="flex between primaryColor center" style={{ fontSize: 20, margin: 10 }}>
            <i onClick={() => setProfile("all")} className="fas fa-arrow-left pointer" />
            <h3 style={{ marginBottom: 10 }}>Minhas Avaliações</h3>
            <div />
          </div>
          {dataR.ratings.map((rate, index) => (
            <div className="flex full" style={{ marginTop: 10 }} key={index}>
              <img src={rate.evaluator.photo} alt="profile" className={style.problemPhoto} />
              <div>
                <div className="flex">
                  <p className={`${style.content} bold`}>{rate.evaluator.name}</p>
                </div>
                <p className={`${style.content} textPrimaryColor`}>{rate.comment}</p>
              </div>
              <p className={`${style.content} primaryColor bold left flex center`}>
                <i className="fas fa-star starsColor " /> {rate.note}
              </p>
            </div>
          ))}
        </div>
      ) : null }
    </div>
  );
};

export default withApollo({ ssr: true })(About);
