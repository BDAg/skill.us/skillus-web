import React, { useState } from "react";
import Router from "next/router";
import { useQuery } from '@apollo/react-hooks';
import style from "./Ranking.module.css";
import SelectSkill from "../Skill/SelectSkill";
import { skillsQuery } from '../../graphql/skills';

const ListUserSkill = ({ users }) => {
	const { data: dataR, error: errorR, loading: loadingR } = useQuery(skillsQuery);
  const [ skills, setSkills ] = useState([]);
  
	if (loadingR) {
		return null;
	}

	if (errorR) {
		return <Error message="🐿 Ocorreu um erro :(" />;
  }
  
  return (
      <div className={style.listUsers}>
        <div style={{margin: "0 10px"}}>
			    <SelectSkill skills={dataR.skills} idSkills={skills} setSkill={setSkills} lengthArraySkill={1}/>
        </div>
        {
          skills.length > 0 ?
            users.map((user, index) => (
              <div
                className={`${style.userInfo} cardBorder flex center pointer`}
                key={user.id}
                onClick={() => Router.push(`/profile/${user.id}`)}>
                <div className={style.ranking}>{index + 1}°</div>
                <div className={`${style.user} flex row center`}>
                  <img src={user.photo} />
                  <div style={{ marginLeft: 10 }}>
                    <div className={style.name}>{user.name}</div>
                  </div>
                </div>
                <div className={`${style.description} textJustify`}>{user.description}</div>
                <div className={style.skill}>
                  <span className={style.skillContainer}>
                    <div>
                      <p className={style.total}>{user.skill[0]?.rating}</p>
                      <img src={"img/NutColor.png"} style={{height: 32, width: 32, marginLeft: 12}}/>
                    </div>
                  </span>
                </div>
              </div>
          ))
          : <p className={style.withoutSkill}>Selecione uma Skill<br/>para ver o ranking</p>
        }
      </div>
  );
};

export default ListUserSkill;
