import React from "react";
import Router from "next/router";
import style from "./Ranking.module.css";

const ListUsers = ({ users }) => {
  return (
    <div className={style.listUsers}>
      {users?.map((user, index) => 
      user.soma > 0?user.status == "Locked_Account" ? null :(
        <div
          className={`${style.userInfo} cardBorder flex center pointer`}
          key={user.id}
          onClick={() => Router.push(`/profile/${user.id}`)}>
          <div className={style.ranking}>{user.position}°</div>
          <div className={`${style.user} flex row center`}>
            <img src={user.photo} />
            <div style={{ marginLeft: 10 }}>
              <div className={style.name}>{user.name}</div>
            </div>
          </div>
          <div className={style.description}>{user.description}</div>
          <div className={style.skill}>
            <div>
              <div className={style.total}>{user.soma}</div>
              <img src={"img/NutColor.png"} style={{height: 32, width: 32, marginLeft: 12}}/>
            </div>
          </div>
        </div>
        
      ):null)}
    </div>
  );
};

export default ListUsers;
