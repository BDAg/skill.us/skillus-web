/* eslint-disable react/display-name */
export default ({ children }) => {
  const router = children.props.children;
  const page = children.props.className;

  let className = children.props.className || "";

  if (router === page) {
    className = `${className} selectedProfile`;
  }

  return <div>{React.cloneElement(children, { className })}</div>;
};
