import React, { useState } from "react";
import style from "../Problem/Problem.module.css";

const SelectSkill = ({
  skills,
  setSkill,
  idSkills,
  adicionalStyle,
  lengthArraySkill = lengthArraySkill ? lengthArraySkill : 5
  }) => {
  const [selectedSkill, setSelectedSkill] = useState([]);

  function skillPush(name) {
    setSelectedSkill([...selectedSkill, name]);
  }
  const selectedId = (e) => skills.find((item) => item.name == e);
  function skillRemove(name) {
    if (selectedSkill.includes(name)) {
      setSelectedSkill(selectedSkill.filter((item) => item !== name));
    }
  }

  return (
    <label>
      <h3 style={adicionalStyle?.title}>Selecione {lengthArraySkill} skills</h3>
      {selectedSkill.length !== 0 ? (
        <div className={`${style.listSkill} flex`}>
          {selectedSkill.map((skill, index) => (
            <p
              className="skillTag"
              style={{ marginRight: 10 }}
              key={index}
              onClick={() => {
                skillRemove(skill);
                const idSelecionado = selectedId(skill);
                idSkills
                  ? setSkill(idSkills.filter((item) => item !== parseInt(idSelecionado.id)))
                  : null;
              }}>
              {skill} <i className="fas fa-times" />
            </p>
          ))}
        </div>
      ) : null}
      <select
        className={`${style.inputForm} full`}
        style={adicionalStyle?.inputForm}
        name="select"
        onChange={(e) => {
          if (!selectedSkill.includes(e.target.value) && selectedSkill.length < lengthArraySkill) {
            skillPush(e.target.value);
            const idSelecionado = selectedId(e.target.value);
            setSkill([...idSkills, parseInt(idSelecionado.id)]);
          }
        }}>
        <option disabled selected value>
          -- Selecione uma skill --
        </option>
        {skills.map((skill) => (
          <option key={skill.id} id={skill.id} value={skill.name}>
            {skill.name}
          </option>
        ))}
      </select>
      <div style={{ marginBottom: 24 }} />
    </label>
  );
};

export default SelectSkill;
