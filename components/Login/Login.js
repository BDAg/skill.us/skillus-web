import Link from "next/link";
import Input from "../Input/Input";
import React, { useState, useContext, useEffect, useCallback } from "react";
import { useRouter } from "next/router";
import { useMutation } from "@apollo/react-hooks";
import { loginQuery } from "../../graphql/loginQuery";
import { withApollo } from "../../lib/apollo";
import { setAccessToken } from "../../accessToken";
import { Me } from "../../graphql/me";
import { UserContext } from "../../context/userContext";
import style from "./Login.module.css";
import ToastContainer from "../Toast/ToastContainer";
import { errorToast } from "../Toast/Toast";

const Login = () => {
  const router = useRouter();
  const { setUser } = useContext(UserContext);

  const [login] = useMutation(loginQuery);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  useEffect(() => {
    router.prefetch("/ranking");
  }, []);

  return (
    <div className={`${style.container} flex`}>
      <ToastContainer />
      <img className={style.image} src="./img/Login.jpg"/>
      <p className={style.textImage}>União de<br />habilidades<br />com a agilidade<br />do esquilo.</p>
      <form
      className={style.containerLogin}
        onSubmit={useCallback(
          async (e) => {
            e.preventDefault();
            try {
              const response = await login({
                variables: {
                  email,
                  password,
                },
                update: (store, { data }) => {
                  if (!data) {
                    return null;
                  }

                  store.writeQuery({
                    query: Me,
                    data: {
                      eu: data.login.user,
                    },
                  });
                },
              });

              if (response.data.login) {
                setUser(response.data.login.user.id);
                setAccessToken(response.data.login.accessToken);
                router.push("/ranking");
              }
            } catch (error) {
              errorToast("🐿 Erro nas credenciais de login !");
              console.log(error.message.slice(-22));
            }
          },
          [email, password]
        )}>
        <h2 className={style.title}>Seja Bem Vindo!</h2>
        <Input
          placeholder="Email"
          icon="fas fa-envelope"
          type="text"
          value={email}
          setValue={setEmail}
        />
        <Input
          placeholder="Senha"
          icon="fas fa-lock"
          type="password"
          value={password}
          setValue={setPassword}
        />
        <button className="colorfulButton" style={{ fontSize: 15, width: "100%", height: 48 }} type="submit">
          Entrar
        </button>
        <Link href={"/register"}>
          <div className={style.register}>
            <p>Ainda não tem uma conta?</p>
            <p className="primaryColor">Cadastre-se agora!</p>
          </div>
        </Link>
      </form>
    </div>
  );
};

export default withApollo({ ssr: true })(Login);
