import React, { useState } from "react";
import styles from "./Register.module.css";
import style from "./Register.module.css";
import Input from "../Input/Input";
import SelectSkills from "../Skill/SelectSkill";
import Link from "next/link";
import { registerUser } from "../../graphql/registerUser";
import { withApollo } from "../../lib/apollo";
import { useMutation, useQuery } from "@apollo/react-hooks";
import { skillsQuery } from "../../graphql/skills";
import { telephoneMask, emailMask } from "./Masks";
import ReactLoading from "react-loading";
import { useRouter } from "next/router";
import ToastyContainer from "../Toast/ToastContainer";
import { errorToast, successToast } from "../Toast/Toast";
import Error from "../Error/Error";

const Register = () => {
  const [register] = useMutation(registerUser);
  const [loadingMutation, setLoadingMutation] = useState(false);
  const { data, error, loading } = useQuery(skillsQuery);
  const [step, setStep] = useState(0);
  const [nome, setNome] = useState("");
  const [email, setEmail] = useState("");
  const [senha, setSenha] = useState("");
  const [confirmarSenha, setConfirmarSenha] = useState("");
  const [telefone, setTelefone] = useState("");
  const [linkedin, setLinkedin] = useState("");
  const [gitlab, setGitlab] = useState("");
  const [descricao, setDescricao] = useState("");
  const photos = ['https://i.imgur.com/0nJ6mOe.png', 'https://i.imgur.com/AbjupGx.png', 'https://i.imgur.com/cpPs0UN.png', 'https://i.imgur.com/Euhyyyp.png', 'https://i.imgur.com/NS1PgdI.png']
  const [photo, setPhoto] = useState("https://i.imgur.com/0nJ6mOe.png");
  const [skills, setSkills] = useState([]);
  const Router = useRouter();
  if (loading) {
    return null;
  }

  if (error) {
    return <Error message="🐿 Ocorreu um erro :(" />;
  }
  const toggleRequiredField = (e) => {
    if (!e.target.value) {
      document.getElementById(`${e.target.id}_em_`).style.display = "block";
    } else {
      document.getElementById(`${e.target.id}_em_`).style.display = "none";
    }
  };

  const validatePassword = (password, confirmPassword) => {
    if (password === confirmPassword) {
      return true;
    } else {
      return false;
    }
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    e.target.checkValidity();
    if (!nome || !senha || !email || !skills) {
      errorToast("🐿 Algum campo está invalido !");
      return false;
    }
    try {
      setLoadingMutation(true);
      const result = await register({
        variables: {
          name: nome,
          email,
          password: senha,
          telephone: telefone,
          description: descricao,
          skill: skills,
          contact: [linkedin, gitlab],
          photo: photo,
        },
      });

      if (!result.data.register) {
        errorToast("🐿 Erro em criar usuario !");
      } else {
        successToast("🐿 Conta criada com sucesso!");
        setTimeout(() => Router.replace("/login"), 2000);
      }
    } catch (error) {
      setLoadingMutation(false);
      errorToast("🐿 Network Error !");
    }
  };

  return (
    <div className={`${styles.container} flex column`}>
      <ToastyContainer />
      <div className={`${styles.registerForm}`}>
        <form
          style={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "space-between",
            width: "40vw",
          }}
          onSubmit={async (e) => await handleSubmit(e)}>
          {step === 0 ? (
            <label>
              <h2 className={styles.title}>Novo usuário</h2>
              <div>
                <Input
                  toggleRequiredField={(e) => toggleRequiredField(e)}
                  value={nome}
                  setValue={setNome}
                  placeholder="Nome"
                  icon="fas fa-user"
                  type="text"
                  required={true}
                />
                <Input
                  toggleRequiredField={(e) => toggleRequiredField(e)}
                  required={true}
                  value={email}
                  setValue={setEmail}
                  placeholder="Email"
                  icon="fas fa-envelope"
                  type="text"
                  specialValidation={emailMask(email)}
                  message={"Insira um email valido"}
                />
                <Input
                  toggleRequiredField={(e) => toggleRequiredField(e)}
                  required={true}
                  value={senha}
                  setValue={setSenha}
                  placeholder="Senha"
                  icon="fas fa-lock"
                  type="password"
                />

                <Input
                  toggleRequiredField={(e) => toggleRequiredField(e)}
                  required={true}
                  value={confirmarSenha}
                  setValue={setConfirmarSenha}
                  specialValidation={validatePassword(senha, confirmarSenha)}
                  placeholder="Confirmar Senha"
                  icon="fas fa-lock"
                  type="password"
                  // message={"Confirme sua senha"}
                />
              </div>
            </label>
          ) : step === 1 ? (
            <label>
              <h2 className={styles.title}>Suas redes</h2>
              <div>
                <Input
                  toggleRequiredField={(e) => toggleRequiredField(e)}
                  required={true}
                  mask={telephoneMask}
                  value={telefone}
                  setValue={setTelefone}
                  placeholder="Telefone"
                  icon="fab fa-whatsapp"
                  type="text"
                />
                <Input
                  toggleRequiredField={(e) => toggleRequiredField(e)}
                  value={linkedin}
                  setValue={setLinkedin}
                  placeholder="Linkedin"
                  icon="fab fa-linkedin-in"
                  type="text"
                />
                <Input
                  toggleRequiredField={(e) => toggleRequiredField(e)}
                  value={gitlab}
                  setValue={setGitlab}
                  placeholder="GitLab"
                  icon="fab fa-gitlab"
                  type="text"
                />
              </div>
            </label>
          ) : (
            <label>
              <h2 className={styles.title}>Conte mais sobre você</h2>
              <div className={style.containerPhotos}>
                {photos.map((link) =>
                  <img
                    onClick={() => setPhoto(link)}
                    style={{
                      opacity: photo === link ? 1 : 0.3,
                    }}
                    src={link}
                  />
                )}
              </div>
              <div>
                <Input
                  toggleRequiredField={(e) => toggleRequiredField(e)}
                  value={descricao}
                  setValue={setDescricao}
                  placeholder="Descrição"
                  icon="far fa-file-alt"
                  type="text"
                />
                <SelectSkills
                  skills={data.skills}
                  idSkills={skills}
                  setSkill={setSkills}
                  adicionalStyle={selectSkills}
                  lengthArraySkill={5}
                />
              </div>
            </label>
          )}
          <div className={styles.bottom}>
            <div className="flex row full between">
              {step === 0 ? (
                <div />
              ) : (
                <div
                  onClick={() => {
                    step > 0 ? setStep(step - 1) : null;
                  }}
                  className="colorfulButton"
                  name="prev">
                  <i className="fas fa-arrow-left" style={{ paddingRight: 10 }} />
                  Voltar
                </div>
              )}
              {step === 2 ? (
                <>
                  {loadingMutation ? (
                    <div className={styles.registerButton}>
                      <ReactLoading type={"spin"} color={"#fff"} height={20} width={20} />
                    </div>
                  ) : (
                    <button type="submit" className={styles.registerButton}>
                      Registrar
                    </button>
                  )}
                </>
              ) : (
                <div
                  onClick={() => {
                    step < 2 ? setStep(step + 1) : null;
                  }}
                  className="colorfulButton"
                  name="next">
                  Próximo
                  <i className="fas fa-arrow-right" style={{ paddingLeft: 10 }} />
                </div>
              )}
            </div>
            <section className="flex row full" style={{ justifyContent: "center" }}>
              {step < 0 ? (
                <p className={styles.steps} onClick={() => setStep(0)}>
                  ➀
                </p>
              ) : (
                <p className={styles.steps} onClick={() => setStep(0)}>
                  ❶
                </p>
              )}
              {step < 1 ? (
                <p className={styles.steps} onClick={() => setStep(1)}>
                  ➁
                </p>
              ) : (
                <p className={styles.steps} onClick={() => setStep(1)}>
                  ❷
                </p>
              )}
              {step < 2 ? (
                <p className={styles.steps} onClick={() => setStep(2)}>
                  ➂
                </p>
              ) : (
                <p className={styles.steps} onClick={() => setStep(2)}>
                  ❸
                </p>
              )}
            </section>
            <div className={`${styles.login} flex column center`}>
              <p>Já possui conta?</p>
              <Link href="/login">
                <p className="bold pointer">Conecte-se!</p>
              </Link>
            </div>
          </div>
        </form>
      </div>
      <img className={style.image} src="./img/Register.jpg" />
    </div>
  );
};

const selectSkills = {
  inputForm: {
    width: "100%",
    height: 40,
    color: "#282828",
    fontSize: 17,
  },
  title: {
    color: "#282828",
    margin: 0,
    fontSize: 25,
    marginBottom: 10,
  },
};

export default withApollo({ ssr: true })(Register);
