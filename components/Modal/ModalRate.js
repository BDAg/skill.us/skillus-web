import React, { useState } from "react";
import { useMutation } from "@apollo/react-hooks";
import { withApollo } from "../../lib/apollo";
import Modal from "react-modal";
import { closeProblem } from "../../graphql/problem";
import { closeUpVotes } from "../../graphql/problem";
import StarRatings from "react-star-ratings";
import style from "../Problem/Problem.module.css";
import ReactLoading from "react-loading";
import { useRouter } from "next/dist/client/router";
import { successToast, errorToast } from "../Toast/Toast";

const ModalComponent = ({ isOpen, setModal, helper, helperID, photo, problemId, skill}) => {
  const [loadingMutation, setloadingMutation] = useState(false);
  const [problem, setProblem] = useState(problem);
  const [comment, setComment] = useState("");
  const [rating, setRating] = useState(0);
  const [close] = useMutation(closeProblem);
  const [closeUp] = useMutation(closeUpVotes);


  const changeRating = (newRating) => {
    setRating(newRating);
  };

  const router = useRouter();

  return (
    <Modal isOpen={isOpen} style={customStyles}>
      <div>
        <div className="full flex center column">
          <p className="bold" style={{ marginBottom: 15 }}>
            Avalie {helper} pela sua habilidade
          </p>
        </div>
        <div className={`cardSimple flex left`} style={{ marginTop: 20 }}>
          <img className={style.photoComment} src={photo} />
          <div className="flex center full" style={{ fontSize: 14 }}>
            <input
              value={comment}
              onChange={(e) => setComment(e.target.value)}
              className="textJustify full"
              style={{ marginRight: 10 }}
              placeholder="Escreva um comentário"
            />
          </div>
        </div>
      </div>
      {loadingMutation ? (
        <div className={style.buttons} style={{ marginRight: 10 }}>
          <div style={{ marginRight: 10 }}>
            <ReactLoading type={"spin"} color={"#f77f5c"} height={30} width={30} />
          </div>
          <div className="disabledButton">Cancelar</div>
          <div className="disabledButton" type="submit">
            Enviar
          </div>
        </div>
      ) : (
        <div style={{ display: "flex", justifyContent: "flex-end", marginRight: 10 }}>
          <div className="borderButton" style={{ marginRight: 10 }} onClick={() => setModal(false)}>
            Cancelar
          </div>
            <button
              className="confirmButton"
              type="submit"
              onClick={async () => {
                const confirmacao = confirm("Deseja enviar essa avaliação?");
                if (confirmacao) {
                  try {
                    setloadingMutation(true);
                    await closeUp({
                      variables: {
                        userId: helperID,
                        skillId: skill,
                      },
                    }),
                    await close({
                      variables: {
                        problemId,
                        note: rating,
                        comment,
                      },
                    }),
                    setloadingMutation(false);
                    router.reload();
                    // successToast("Problema Solucionado");
                  } catch (error) {
                    setloadingMutation(false);
                    errorToast(error);
                  }
                } else {
                  return null;
                }
              }}>
              Avaliar
            </button>
          )
        </div>
      )}
    </Modal>
  );
};

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "50%",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
  },
};

export default withApollo({ ssr: true })(ModalComponent);
