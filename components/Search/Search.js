import React from "react";
import styles from "./Search.module.css";

const Search = ({ matchingWord, setMatchingWord }) => {
  return (
    <div className={`${styles.search} right full`}>
      <input
        className="full"
        type="text"
        value={matchingWord}
        onChange={(e) => setMatchingWord(e.target.value)}
        placeholder="Pesquise aqui"
      />
    </div>
  );
};

export default Search;
