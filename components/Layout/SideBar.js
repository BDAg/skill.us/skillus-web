import React from "react";
import Link from "next/link";
import ActiveLink from "../ActiveLink/ActiveLink";
import style from "./Layout.module.css";
import { Me } from "../../graphql/me";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { withApollo } from "../../lib/apollo";
import { logout } from "../../graphql/logout";
import { useRouter } from "next/router";

const SideBar = () => {
  const { loading, data } = useQuery(Me);
  const [Logout] = useMutation(logout);
  const router = useRouter();

  if (loading) {
    return null;
  }

  return (
    <div className={`${style.sideBar}`}>
      <div className={style.increaseBar} />
      <nav className={style.links}>
        <Link href={"/ranking"}>
          <div className={style.logo}>
            <img src="https://i.imgur.com/2cZJOFU.png" />
            <p className={style.skillus}>Skill.us</p>
          </div>
        </Link>
        <div className={style.backgoundItem}>
          <ActiveLink href="/">
            <div className={style.navItem}>
              <i className="fas fa-home"></i>
              <a>Home</a>
            </div>
          </ActiveLink>
        </div>
        <div className={style.backgoundItem}>
          <ActiveLink href="/ranking">
            <div className={style.navItem}>
              <i className="fas fa-medal"></i>
              <a>Ranking</a>
            </div>
          </ActiveLink>
        </div>
        <div className={style.backgoundItem}>
          <ActiveLink href="/problem">
            <div className={style.navItem}>
              <i className="fas fa-bug"></i>
              <a>Perguntas</a>
            </div>
          </ActiveLink>
        </div>
      </nav>
      <div className={style.user}>
        {data ? (
          <div>
            <div className={`${style.backgoundItem} help`}>
              <a className={style.navItem} href="https://www.google.com">
                <i className="far fa-question-circle"></i>
                <a>Ajuda</a>
              </a>
            </div>
            <div className={style.backgoundItem}>
              <Link href={`/profile/${data.eu.id}`}>
                <div className={style.navItem}>
                  <img className={style.userImg} src={data.eu.photo} />
                  <a>{data.eu.name.split(" ")[0]}</a>
                </div>
              </Link>
            </div>
            <div className={`${style.backgoundItem} logoutIcon`}
              onClick={async () => {
                await Logout();
                router.push('/login');
              }}>
              <div className={style.navItem}>
                <i class="fas fa-sign-out-alt"></i>
                <a>Sair</a>
              </div>
            </div>
          </div>
        ) : (
          <div>
            <div className={`${style.backgoundItem} help`}>
              <a className={style.navItem} href="https://www.google.com">
                <i className="far fa-question-circle"></i>
                <a>Ajuda</a>
              </a>
            </div>
            <div className={style.backgoundItem}>
              <Link href={"/login"}>
                <div className={style.navItem}>
                  <i className="fas fa-user-circle"></i>
                  <a>Entrar</a>
                </div>
              </Link>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default withApollo({ ssr: true })(SideBar);
