import React, { Component } from "react";
import SideBar from "./SideBar";
import BottomBar from "./BottomBar";
import Head from "next/head";
import Router from "next/router";
import NProgress from "nprogress";

Router.events.on("routeChangeStart", () => NProgress.start());
Router.events.on("routeChangeComplete", () => NProgress.done());
Router.events.on("routeChangeError", () => NProgress.done());

const Layout = (Page) => {
  return class extends Component {
    render() {
      return (
        <div>
          <Head>
            <title>Skill.us</title>
            <link rel="shortcut icon" href="/assets/favicon.png" />
            <meta property="og:title" content="Skill.us" key="title" />
          </Head>
          <SideBar />
          <Page />
          {Page.name == "LoginScreen" || Page.name == "HomePage" || Page.name == "Custom404" ? <BottomBar /> : null}
        </div>
      );
    }
  };
};

export default Layout;
