import React, { useState } from "react";
import Link from 'next/link';
import style from "./Header.module.css";
import Search from "../Search/Search";
import { logout } from '../../graphql/logout';
import { useMutation } from "@apollo/react-hooks";
import { useRouter } from "next/router";
import { withApollo } from "../../lib/apollo";

const Header = ({
	title,
	search,
	logoutIcon,
	edit,
	problemNew,
	problemStatus,
	searchWord,
	setSearchWord,
	setStateValue
	}) => {
	
	const [Logout] = useMutation(logout);
	const router = useRouter();
	const [searchClick, setSearchClick] = useState(false);
	const [stateSelect, setStateSelect] = useState('none');
	const [stateStyle, serStateStyle] = useState({backgroundColor: "#fff",border: "solid 2px #F77F5C"});


	if (searchClick === false) {
		return (
			<div className={style.header}>
				<div className={style.title}>
					<p>{title}</p>
				</div>
				<div className={style.containerOptions}>
					{logoutIcon ?
							<div className={style.divs}
								onClick={async () => {
									await Logout();
									router.push('/login');
								}}>
								<div className={style.item}>
									<p>Sair</p>
									<i class="fas fa-sign-out-alt"></i>
								</div>
							</div>
					: null }
					{edit ?
						<Link 
							href={{
								pathname: "/profile/edit",
								query: {id: router.query.id}
							}}
							>
							<div className={style.divs}>
								<div className={style.item}>
									<p>Editar</p>
									<i className="fas fa-pencil-alt" />
								</div>
							</div>
						</Link>
					: null }
					{search ?
						<div className={style.divs} onClick={() => setSearchClick(true)}>
							<div className={style.item}>
								<p>Pesquisar</p>
								<i className="fas fa-search"></i>
							</div>
						</div>
					: null }
					{problemNew ?
						<Link href={"/problem/new"}>
							<div className={style.divs}>
								<div className={style.item}>
									<p>Novo</p>
									<i class="fas fa-plus-circle"></i>
								</div>
							</div>
						</Link>
					: null }
					{problemStatus ?
						<div className={style.divs}>
							<div className={style.item} onClick={stateSelect === 'none' ? () => setStateSelect('flex') : () => setStateSelect('none') }>
								<p>Status</p>
								<div className={style.stateSelected} style={stateStyle}/>
							</div>
							<div style={{display: stateSelect}}>
								<div className={style.selectState} onClick={() => setStateSelect('none')}>
									<div className={style.containerState} onClick={() => {
										setStateValue('all');
										serStateStyle({backgroundColor: "#fff",border: "solid 2px #F77F5C"})}}>
										<p>todos</p>
										<div className={style.state} style={{backgroundColor: "#fff",border: "solid 2px #F77F5C"}}/>
									</div>
									<div className={style.containerState} onClick={() => {
										setStateValue('Aguardando ajuda');
										serStateStyle({backgroundColor: "#68F75C"})}}>
										<p>Aguardando Ajuda</p>
										<div className={style.state} style={{backgroundColor: "#68F75C"}}/>
									</div>
									<div className={style.containerState} onClick={() => {
										setStateValue('Sendo resolvido');
										serStateStyle({backgroundColor: "#F4F75C"})}}>
										<p>Sendo Resolvido</p>
										<div className={style.state} style={{backgroundColor: "#F4F75C"}}/>
									</div>
									<div className={style.containerState} onClick={() => {
										setStateValue('Solucionado');
										serStateStyle({backgroundColor: "#5C7EF7"})}}>
										<p>Solucionado</p>
										<div className={style.state} style={{backgroundColor: "#5C7EF7"}}/>
									</div>
								</div>
							</div>
						</div>
					: null }
				</div>
			</div>
		);
	} else {
		return (
			<div className={style.header}>
				<div className={`full ${style.divs}`}>
					<div className={`full ${style.item}`}>
						<p className="right">Pesquisar</p>
        				<Search matchingWord={searchWord} setMatchingWord={setSearchWord} />
					</div>
				</div>
				<div className={style.divs} onClick={() => {setSearchClick(false); setSearchWord('')}}>
					<div className={style.item}>
						<p>Cancelar</p>
						<i className="fas fa-times"></i>
					</div>
				</div>
			</div>
		);
	}
};

export default withApollo({ ssr: true })(Header);
