import React from "react";

const Error = ({ message }) => {
  return (
    <div className="errorMessage">
      {message}
    </div>
  );
};

export default Error;
