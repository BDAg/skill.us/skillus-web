import React, { useState } from 'react';
import Link from 'next/link';
import style from './Problem.module.css';
import SelectSkills from '../Skill/SelectSkill';
import { useMutation, useQuery } from '@apollo/react-hooks';
import Input from '../Input/Input';
import { CreateProblem } from '../../graphql/createProblem';
import { Me } from '../../graphql/me';
import { Problems } from '../../graphql/problem';
import { skillsQuery } from '../../graphql/skills';
import { withApollo } from '../../lib/apollo';
import ReactLoading from 'react-loading';
import { successToast, errorToast } from '../Toast/Toast';
import Error from '../Error/Error';

const NewProblem = ({ close }) => {
	const { loading, data } = useQuery(Me);
	const { data: dataR, error: errorR, loading: loadingR } = useQuery(skillsQuery);
	const [ createProblem ] = useMutation(CreateProblem);
	const [ assunto, setAssunto ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ skills, setSkills ] = useState([]);
	const [ mr, setMr ] = useState(false);
	const [ loadingSubmit, setLoadingSubmit ] = useState(false);
	if (loading || loadingR) {
		return null;
	}

	if (errorR) {
		return <Error message="🐿 Ocorreu um erro :(" />;
	}

	return (
		<form
			onSubmit={async (e) => {
				e.preventDefault();
				setLoadingSubmit(true);
				try {
					await createProblem({
						variables: {
							name: assunto,
							description,
							createdBy: parseInt(data.eu.id),
							skill: skills,
							multi_response: mr
						},
						update(cache, { data: { createProblem } }) {
							const { problems } = cache.readQuery({ query: Problems });
							cache.writeQuery({
								query: Problems,
								data: { problems: problems.concat([ createProblem ]) }
							});
						}
          });
					setLoadingSubmit(false);
					// successToast("Problema Criado com sucesso");
					close(true);
				} catch (error) {
					setLoadingSubmit(false);
					errorToast(error);
				}
			}}
			className={`${style.form} flex column`}
		>
			<div className="flex center" style={{ margin: '24px 0' }}>
				<img className={style.photo} src={data.eu.photo} style={{ marginLeft: 0 }} />
				<div>
					<p className="bold">criador</p>
					<p style={{ fontSize: 24 }}>{data.eu.name}</p>
				</div>
			</div>
			<Input placeholder="Título da pergunta" type="text" value={assunto} setValue={setAssunto} icon={false} />
			<SelectSkills skills={dataR.skills} idSkills={skills} setSkill={setSkills} />
			<div>
				<label className="switch">
					Permitir multiplas respostas:
					<input
						value={mr}
						onChange={() => {
							setMr(!mr)
						}}
						type="checkbox"
					/>
				</label>
			</div>
			<br />
			<Input
				placeholder="Descrição da pergunta"
				type="textArea"
				value={description}
				setValue={setDescription}
				icon={false}
			/>
			{loadingSubmit ? (
				<div className={style.buttons}>
					<div style={{ marginRight: 10 }}>
						<ReactLoading type={'spin'} color={'#f77f5c'} height={30} width={30} />
					</div>
					<Link href={'/problem'}>
						<div className="borderButton"> Cancelar </div>
					</Link>
					<div className="disabledButton" type="submit">
						Enviar
					</div>
				</div>
			) : (
				<div className={style.buttons}>
					<Link href={'/problem'}>
						<div className="borderButton"> Cancelar </div>
					</Link>
					<button className="confirmButton" type="submit">
						Enviar
					</button>
				</div>
			)}
		</form>
	);
};

export default withApollo({ ssr: true })(NewProblem);
