import React, { useState } from "react";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { withApollo } from "../../lib/apollo";
import style from "./Problem.module.css";
import Modal from "../Modal/ModalRate";
import ReactLoading from "react-loading";
import { useRouter } from "next/dist/client/router";
import { ProblemHelper, RemoveProblemHelper } from "../../graphql/problemHelper";
import { Me } from "../../graphql/me";
import Comments from "./Comments";
import { successToast, errorToast } from "../Toast/Toast";
import ToastyContainer from "../Toast/ToastContainer";
import Link from "next/link";
import Error from "../../components/Error/Error";

const Problem = ({ problem }) => {
  const { loading, error, data } = useQuery(Me);
  const [AddProblemHelper] = useMutation(ProblemHelper);
  const [RemomveHelper] = useMutation(RemoveProblemHelper);
  const [loadingMutation, setloadingMutation] = useState(false);
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const router = useRouter();

  if (loading) {
    return null;
  }

  // if (error) {
  //voce precisa logar irmao
  //   console.log(error);
  //   return <Error message="🐿 Ocorreu um erro ao lista o Problema :(" />;
  // }
  if (problem) {
    return (
      <div className="flex column">
        {/* {error && } */}
        <ToastyContainer />
        <div className="flex" style={{ margin: 10 }}>
          <p className="primaryColor bold">#{problem.id}&nbsp;</p>
          <p className="bold">{problem.name}</p>
          <div className="left" style={{ fontSize: 15, color: problem.color }}>
            <i className={`fas fa-${problem.icon}`} />
            <span style={{ marginLeft: 5 }}>{problem.status}</span>
          </div>
        </div>
        <div className="flex">
          <div className="flex right">
            {problem.skill.map((s) => (
              <p key={s.id} className="skillTag" style={{ marginLeft: 10 }}>
                {s.name}
              </p>
            ))}
          </div>
          <Link href={`/profile/${problem?.helper?.id}`}>
            <a className="flex left" title={problem?.helper?.name}>
              <img className={style.helperPhoto} src={problem?.helper?.photo} />
            </a>
          </Link>
        </div>
        <div className="cardSimple flex">
          <div>
            <img className={style.photo} src={problem?.creator?.photo} />
            <p className="bold textCenter" style={{ width: 110 }}>
              {problem?.creator?.name}
            </p>
          </div>
          <p className="textJustify" style={{ marginLeft: 10 }}>
            {problem.description}
          </p>
        </div>
        <div>
          <Comments
            problemId={problem.id}
            creator={problem.creator}
            helper={problem.helper}
            status={problem.status}
            user={data?.eu.name}
          />
          {problem.status === "Sendo resolvido" && data?.eu.name === problem.creator.name ? (
            <div style={{ display: "flex", justifyContent: "flex-end", marginRight: 10 }}>
              <Modal
                isOpen={modalIsOpen}
                setModal={setModalIsOpen}
                helper={problem.helper.name}
                helperID={problem.helper.id}
                skill={problem.skill[0].id}
                photo={problem.creator.photo}
                problemId={problem.id}
              />
              <div
                className="borderButton flex"
                style={{ marginRight: 10 }}
                onClick={async () => {
                  const confirmacao = confirm("Deseja pedir outra opnião?");
                  if (confirmacao) {
                    try {
                      setloadingMutation(true);
                      await RemomveHelper({
                        variables: { problemId: problem.id },
                      });
                      setloadingMutation(false);
                      router.reload();
                    } catch (error) {
                      setloadingMutation(false);
                    }
                  } else {
                    return null;
                  }
                }}>
                {loadingMutation ? (
                  <ReactLoading type={"spin"} color={"red"} height={15} width={15} />
                ) : (
                  <p>Pedir outra opnião</p>
                )}
              </div>
              <div className="confirmButton flex" onClick={() => setModalIsOpen(true)}>
                Avaliar resposta
              </div>
            </div>
          ) : null}
        </div>
        <div style={{ display: "flex", justifyContent: "flex-end", marginRight: 10 }}>
          {problem.status === "Aguardando ajuda" && data?.eu.name !== problem.creator?.name ? (
            <div
              className="confirmButton flex"
              onClick={async () => {
                const confirmacao = confirm("Deseja entrar nesse problema?");
                if (confirmacao && data.eu.id !== undefined) {
                  try {
                    setloadingMutation(true);
                    await AddProblemHelper({
                      variables: {
                        problemId: problem.id,
                        userId: data.eu.id,
                      },
                    });
                    setloadingMutation(false);
                    // successToast("Você entrou em um problema com sucesso");
                    router.reload();
                  } catch (error) {
                    setloadingMutation(false);
                    errorToast("Algo deu errado!");
                  }
                } else {
                  setloadingMutation(false);
                  errorToast("Algo deu errado!");
                  return null;
                }
              }}>
              {loadingMutation ? (
                <ReactLoading type={"spin"} color={"#fff"} height={15} width={15} />
              ) : (
                <p>Ajudar</p>
              )}
            </div>
          ) : null}
        </div>
      </div>
    );
  }
  return null;
};

export default withApollo({ ssr: true })(Problem);
