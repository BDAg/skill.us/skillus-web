import React from "react";
import Router from "next/router";
import style from "./Problem.module.css";

const ListProblem = ({ problemList }) => {
  return (
    <div>
      {problemList.reverse().map((problem) => (
        <div
          className={`${style.card} flex cardBorder`}
          onClick={() => Router.push(`/problem/${problem.id}`)}
          key={problem.id}>
          <div>
            <img className={style.photo} src={problem?.creator?.photo} />
            <p className="bold textCenter" style={{ width: 110 }}>
              {problem?.creator?.name}
            </p>
          </div>
          <div className={`${style.container} transition`}>
            <div className="flex">
              <div className="flex">
                <p className="primaryColor bold">#{problem.id}&nbsp;</p>
                <p className="bold">{problem.name}</p>
              </div>
              <div className="left" style={{ fontSize: 15, color: problem.color }}>
                <i className={`fas fa-${problem.icon}`} />
                <span style={{ marginLeft: 5 }}>{problem.status}</span>
              </div>
            </div>
            <p className={`${style.content} textJustify`}>{problem.description}</p>
          </div>
        </div>
      ))}
    </div>
  );
};

export default ListProblem;
