import React from "react";
import style from "./Problem.module.css";
import Link from "next/link";

const ListProblemCard = ({ problemList, amount }) => {
  if (problemList) {
    return (
      <>
        {problemList
          .reverse()
          .slice(0, amount)
          .map((problem, index) => (
            <Link href={`/problem/${problem.id}`} key={index}>
              <div className="flex full pointer" style={{ marginTop: 10, marginBottom: 10 }}>
                <img src={problem?.creator?.photo} alt="profile" className={style.problemPhoto} />
                <div>
                  <div className="flex center">
                    <p className="primaryColor bold">#{problem?.id}&nbsp;</p>
                    <p className={`${style.content} bold`}>{problem?.name}</p>
                  </div>
                  <p className={`${style.content} textPrimaryColor`}>{problem?.description}</p>
                </div>
              </div>
            </Link>
          ))}
      </>
    );
  }

  return null;
};

export default ListProblemCard;
