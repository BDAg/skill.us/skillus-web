/* eslint-disable no-undef */
import { InMemoryCache } from "apollo-cache-inmemory";
import { HttpLink } from "apollo-link-http";
import { ApolloLink, Observable } from "apollo-link";
import { ApolloClient } from "apollo-client";
import { onError } from "apollo-link-error";
import { TokenRefreshLink } from "apollo-link-token-refresh";
import jwtDecode from "jwt-decode";
import { getAccessToken, setAccessToken } from "./accessToken";

export default function createApolloClient(initialState, ctx) {
  const requestLink = new ApolloLink(
    (operation, forward) =>
      new Observable((observer) => {
        let handle;
        Promise.resolve(operation)
          .then((operation) => {
            const accessToken = getAccessToken();
            if (accessToken) {
              operation.setContext({
                headers: {
                  authorization: `bearer ${accessToken}`,
                },
              });
            }
          })
          .then(() => {
            handle = forward(operation).subscribe({
              next: observer.next.bind(observer),
              error: observer.error.bind(observer),
              complete: observer.complete.bind(observer),
            });
          })
          .catch(observer.error.bind(observer));

        return () => {
          if (handle) handle.unsubscribe();
        };
      })
  );

  return new ApolloClient({
    ssrMode: Boolean(ctx),
    link: ApolloLink.from([
      new TokenRefreshLink({
        accessTokenField: "accessToken",
        isTokenValidOrUndefined: () => {
          const token = getAccessToken();
          if (!token) {
            return true;
          }

          try {
            const { exp } = jwtDecode(token);
            if (Date.now() >= exp * 1000) {
              return false;
            } else {
              return true;
            }
          } catch {
            return false;
          }
        },
        fetchAccessToken: () => {
          return fetch(process.env.API_REFRESH_TOKEN, {
            method: "POST",
            credentials: "include",
          });
        },
        handleFetch: (accessToken) => {
          setAccessToken(accessToken);
        },
        handleError: (err) => {
          console.warn("Seu token é invalido, tente relogar");
          console.error(err);
        },
      }),
      onError(({ graphQLErrors, networkError }) => {
        // console.log(graphQLErrors);
        // console.log(networkError);
      }),
      requestLink,
      new HttpLink({
        uri: process.env.API,
        credentials: "include",
      }),
    ]),
    cache: new InMemoryCache().restore(initialState),
  });
}
