import React from "react";
import Layout from "../components/Layout/Layout";
import Login from "../components/Login/Login";

const LoginScreen = () => {
  return (
    <Login />
  );
};

export default Layout(LoginScreen);
