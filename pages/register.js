import React from "react";
import Layout from "../components/Layout/Layout";
import Register from "../components/Register/Register";

const RegisterSteps = () => {
  return <Register />;
};

export default Layout(RegisterSteps);
