import React, { useState } from 'react';
import Layout from '../../components/Layout/Layout';
import Header from '../../components/Layout/Header';
import { useQuery } from "@apollo/react-hooks";
import { withApollo } from "../../lib/apollo";
import { userAbout } from "../../graphql/userAbout";
import Edit from '../../components/Profile/Edit';
import { useRouter } from "next/router";

const EditPage = () => {
    const router = useRouter();
    const { loading, error, data } = useQuery(userAbout, {
        variables: {
            id: router.query.id,
        },
    });

    if (loading) {
        return null;
    }

    if (error) {
        return <Error message="🐿 Ocorreu um erro ao listar o Problema :(" />;
    }

    if (router.query.id) {
        return (
            <>
                <Header
                    title={"Editar Perfil"}
                    save={true}
                />
                <main>
                    <Edit data={data}/>
                </main>
            </>
        )
    } else {
        return null
    }
}

export default withApollo({ ssr: true })(Layout(EditPage));
