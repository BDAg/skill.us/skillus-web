import React from "react";
import { useRouter } from "next/router";
import Layout from "../../components/Layout/Layout";
import About from "../../components/Profile/About";
import { userAbout } from "../../graphql/userAbout";
import { withApollo } from "../../lib/apollo";
import { useQuery } from "@apollo/react-hooks";
import Error from "../../components/Error/Error";
import Header from "../../components/Layout/Header";
import { Me } from "../../graphql/me";

const Profile = () => {
  const router = useRouter();
  const { loading: loadingMe, data: dataMe } = useQuery(Me);
  const { loading, error, data } = useQuery(userAbout, {
    variables: {
      id: router.query.id,
    },
  });

  if (loading || loadingMe) {
    return null;
  }

  if (error) {
    return <Error message="🐿 Ocorreu um erro ao listar o Problema :(" />;
  }

  return (
    <>
      <Header
        title={"Perfil"}
        logoutIcon={true}
        edit={router.query.id === dataMe.eu.id ? true : false}
      />
      <main>
        <div className="componentsUser">
          <About userData={data} />
        </div>
      </main>
    </>
  );
};

export default withApollo({ ssr: true })(Layout(Profile));
