/* eslint-disable no-undef */
import "@fortawesome/fontawesome-free/css/all.min.css";
import "../public/css/app.css";
import "react-toastify/dist/ReactToastify.css";
import "../public/css/nprogess.css";
import { useEffect, useState, useMemo } from "react";
import { setAccessToken } from "../accessToken";
import { UserContext } from "../context/userContext";
import { IsLoggedIn } from "../context/isLoggedIn";

export default function MyApp({ Component, pageProps }) {
  const [user, setUser] = useState("");
  const [loading, setLoading] = useState(true);
  const [logged, setLogged] = useState(false);
  const valueUser = useMemo(() => ({ user, setUser }), [user, setUser]);
  const valueLogged = useMemo(() => ({ logged, setLogged }), [logged, setLogged]);

  useEffect(() => {
    fetch(process.env.API_REFRESH_TOKEN, {
      method: "POST",
      credentials: "include",
    }).then(async (response) => {
      const { accessToken } = await response.json();
      setAccessToken(accessToken);
      setLoading(false);
    });
  }, []);

  if (loading) {
    return null;
  }

  return (
    <IsLoggedIn.Provider value={valueLogged}>
      <UserContext.Provider value={valueUser}>
        <Component {...pageProps} />{" "}
      </UserContext.Provider>
    </IsLoggedIn.Provider>
  );
}
