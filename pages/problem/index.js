import React, { useState, useContext } from "react";
import Skeleton from "react-loading-skeleton";
import Layout from "../../components/Layout/Layout";
import ListProblem from "../../components/Problem/ListProblem";
import { useQuery } from "@apollo/react-hooks";
import { Problems } from "../../graphql/problem";
import { withApollo } from "../../lib/apollo";
import Error from "../../components/Error/Error";
import Header from "../../components/Layout/Header";

const ProblemPage = () => {
  const { loading, error, data } = useQuery(Problems);
  const [searchWord, setSearchWord] = useState("");
  const searchString = searchWord.trim().toLowerCase();
	const [stateValue, setStateValue] = useState('all');
  RegExp.quote = function (str) {
    return str.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
  };
  const matchingRegex = new RegExp(RegExp.quote(searchString));

  if (error) {
    return <Error message="🐿 Ocorreu um erro ao listar o Problema :(" />;
  }

  return (
    <>
      <Header
        title={"Perguntas"}
        problemNew={true}
        search={true}
        searchWord={searchWord}
        setSearchWord={setSearchWord}
        problemStatus={true}
        stateValue={stateValue}
        setStateValue={setStateValue}
      />
      <main>
        {loading ? (
          <Skeleton style={{ borderRadius: 10, marginBottom: 10 }} count={6} height={86} />
        ) : (
          <>
            {data !== undefined ? (
              <ListProblem
                problemList={data.problems.filter((problem) => {
                  if (stateValue === "all") {
                    return problem.name.toLowerCase().match(matchingRegex);
                  }
                  return problem.status == stateValue && problem.name.toLowerCase().match(matchingRegex);
                })}
              />
            ) : (
              <Error message="Ocorreu um erro ao listar as perguntas :(" />
            )}
          </>
        )}
      </main>
    </>
  );
};

export default withApollo({ ssr: true })(Layout(ProblemPage));
