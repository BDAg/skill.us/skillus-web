import React from "react";
import { useRouter } from "next/router";
import { useQuery } from "@apollo/react-hooks";
import { withApollo } from "../../lib/apollo";
import { problem } from "../../graphql/problem";
import Problem from "../../components/Problem/Problem";
import Layout from "../../components/Layout/Layout";
import Error from "../../components/Error/Error";

const ProblemScreen = () => {
  const router = useRouter();
  const { error, loading, data } = useQuery(problem, {
    variables: {
      id: router.query.id,
    },
  });

  if (loading) {
    return null;
  }

  if (data.problem) {
    return (
      <main className="flex column">
        <Problem problem={data.problem} />
      </main>
    );
  }

  return (
    <main>
      <Error message="🐿 Não foi encontrado um problema com esse ID :(" />
    </main>
  );
};

export default withApollo({ ssr: true })(Layout(ProblemScreen));
