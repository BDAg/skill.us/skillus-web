import React from 'react';
import Layout from '../../components/Layout/Layout';
import Header from '../../components/Layout/Header';
import NewProblem from '../../components/Problem/NewProblem';

const NewProblemPage = () => {
    return(
        <>
            <Header title={"Nova Pergunta"}/>
            <main>
                <NewProblem />
            </main>
        </>
    )
}

export default Layout(NewProblemPage);
