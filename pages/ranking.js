import React, { useState } from "react";
import Skeleton from "react-loading-skeleton";
import Layout from "../components/Layout/Layout";
import ListUsers from "../components/Ranking/ListUsers";
import ListUserSkill from "../components/Ranking/ListUserSkill";
import Header from "../components/Layout/Header";
import { withApollo } from "../lib/apollo";
import { listUsers } from "../graphql/users";
import { useQuery } from "@apollo/react-hooks";

const Ranking = () => {
  const { loading, error, data } = useQuery(listUsers);
  const [filter, setFilter] = useState("users");
  const [searchWord, setSearchWord] = useState("");

  if (loading) {
    return null;
  }

  const searchString = searchWord.trim().toLowerCase();

  RegExp.quote = function (str) {
    return str.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
  };

  const matchingRegex = new RegExp(RegExp.quote(searchString));

  const filteredArrayBySkill = data?.users?.sort((a, b) => b.soma - a.soma).filter((user, index) => {
    user.position = index + 1;
    const skillFiltered = user.skill.filter((skill) => {
      return skill.name.toLowerCase().match(matchingRegex);
    });
    if (skillFiltered.length > 0) {
      return skillFiltered;
    }
  });

  return (
    <>
      <Header
        title={"Ranking"}
        search={true}
        searchWord={searchWord}
        setSearchWord={setSearchWord}
      />
      <main>
        <div className="flex between full" style={{ padding: "0px 0px 10px 0px" }}>
          <div className="selectRanking flex full" style={{justifyContent: "space-around"}}>
            <label onChange={() => setFilter("users")} style={{ width: "50%" }}>
              <input className="radio" type="radio" name="ranking" value="users" defaultChecked />
              <div className="radioDiv"> Usuários com mais nozes </div>
            </label>
            <label onChange={() => setFilter("skills")} style={{ width: "50%" }}>
              <input className="radio" type="radio" name="ranking" value="skills" />
              <div className="radioDiv"> Habilidades com mais nozes </div>
            </label>
          </div>
        </div>
        <div className="aa">
          {loading ? (
            <Skeleton style={{ borderRadius: 10, marginBottom: 10 }} count={6} height={106} />
          ) : (
            <>
              {filter === "users" ? (
                <ListUsers
                  users={data?.users?.filter((user) => {
                    return user.name.toLowerCase().match(matchingRegex);
                  })}
                />
              ) : (
                <ListUserSkill users={filteredArrayBySkill} />
              )}
            </>
          )}
        </div>
      </main>
    </>
  );
};

export default withApollo({ ssr: true })(Layout(Ranking));
