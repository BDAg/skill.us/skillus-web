import gql from "graphql-tag";

export const Problems = gql`
  query {
    problems {
      id
      name
      description
      status
      icon
      color
      skill {
        id
        name
      }
      creator {
        id
        photo
        name
      }
    }
  }
`;

export const problem = gql`
  query Problem($id: ID!) {
    problem(id: $id) {
      id
      name
      description
      status
      icon
      color
      skill {
        id
        name
      }
      creator {
        id
        photo
        name
      }
      helper {
        id
        photo
        name
      }
    }
  }
`;

export const closeProblem = gql`
   mutation CloseProblem($problemId: ID!, $note: Float!, $comment: String!) {
     closeProblem(problem_id: $problemId, note: $note, comment: $comment) {
       id
     }
   }
 `;

export const closeUpVotes = gql`
  mutation UpdateUserSkillRating($userId: ID!, $skillId: ID!) {
    updateUserSkillRating(user_Id: $userId, skill_Id: $skillId)
  }`;

export const problemByUser = gql`
  query ProblemsByUser($userId: ID!) {
    problemsByUser(user_id: $userId) {
      name
      id
      status
      icon
      color
      description
      helper {
        id
        name
        photo
      }
      creator {
        photo
        id
        name
      }
    }
  }
`;
