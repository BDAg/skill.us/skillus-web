import gql from "graphql-tag";

export const registerUser = gql`
  mutation Register(
    $name: String!
    $email: String!
    $password: String!
    $telephone: String
    $description: String
    $skill: [Int]
    $contact: [String]
    $photo: String
  ) {
    register(
      name: $name
      email: $email
      password: $password
      telephone: $telephone
      description: $description
      skill: $skill
      contact: $contact
      photo: $photo
    )
  }
`;
