import gql from "graphql-tag";

export const userAbout = gql`
  query userAbout($id: ID!) {
    user(id: $id) {
      id
      name
      email
      telephone
      soma
      photo
      description
      contact {
        id
        name
        value
      }
      skill {
        id
        name
        rating
      }
    }
  }
`;

export const editUser = gql`
  mutation UpdateProfile(
    $id: ID!,
    $name: String,
    $email: String,
    $telephone: String,
    $description: String,
    $skill: [Int],
    $contact: [String],
    $photo: String,
    $user_status_id: Int
  ) {
    updateProfile(
      id: $id,
      name: $name,
      email: $email,
      telephone: $telephone,
      description: $description,
      skill: $skill,
      contact: $contact,
      photo: $photo,
      user_status_id: $user_status_id
    )
  }
`;
