import gql from "graphql-tag";

export const ProblemHelper = gql`
  mutation AddProblemHelper($problemId: ID!, $userId: ID!) {
    addProblemHelper(problem_id: $problemId, user_id: $userId) {
      id
    }
  }
`;

export const RemoveProblemHelper = gql`
  mutation RemomveHelper($problemId: ID!) {
    removeProblemHelper(problem_id: $problemId) {
      id
    }
  }
`;
