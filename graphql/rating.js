import gql from "graphql-tag";

export const rating = gql`
  query Ratings($userId: ID!) {
    ratings(user_id: $userId) {
      id
      problem_id
      note
      comment
      evaluator {
        id
        name
        photo
      }
    }
  }
`;
