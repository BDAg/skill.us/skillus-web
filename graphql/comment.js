import gql from "graphql-tag";

export const AddProblemComment = gql`
  mutation AddComment($text: String!, $problemId: ID!, $userIdSender: ID!) {
    addProblemComment(text: $text, problem_id: $problemId, user_id_sender: $userIdSender) {
      id
      text
    }
  }
`;

export const comments = gql`
  query Comments($id: ID!) {
    comments(problem_id: $id) {
      id
      text
      date_creation
      problem_id
      sender {
        id
        name
        photo
      }
    }
    problem(id: $id) {
      id
      description
      rating {
        id
        comment
        note
      }
    }
  }
`;
