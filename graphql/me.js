import gql from "graphql-tag";

export const Me = gql`
  query {
    eu {
      id
      name
      photo
    }
  }
`;
