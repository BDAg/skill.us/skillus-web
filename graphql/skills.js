import gql from "graphql-tag";

export const skillsQuery = gql`
  query Skills {
    skills {
      id
      name
    }
  }
`;
