import gql from "graphql-tag";

export const CreateProblem = gql`
  mutation CreateProblem($name: String, $description: String, $createdBy: Int, $skill: [Int], $multi_response: Boolean) {
    createProblem(name: $name, description: $description, createdBy: $createdBy, skill: $skill, multi_response: $multi_response) {
      id
      name
      description
      status
      color
      icon
      skill {
        id
        name
      }
      creator {
        id
        photo
        name
      }
      multi_response
    }
  }
`;
