import gql from "graphql-tag";

export const listUsers = gql`
  query {
    users {
      id
      name
      description
      soma
      photo
      status
      skill {
        id
        name
        rating
      }
    }
  }
`;
