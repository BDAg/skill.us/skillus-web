<h1>Web Skillus</h1>

## Montagem do Ambiente do Desenvolvimento:

#### Instalando NodeJS e NPM:

- Linux

```sh
sudo apt update
sudo apt-get install nodejs
sudo apt install npm
```

- Windows 10

```sh
Baixar Instalador no site https://nodejs.org/

```

#### Instalando Visual Studio Code (este passo é opcional):

- Neste passo ocorre a instalação do editor de texto usado pelo time

```sh
https://code.visualstudio.com/download

```

#### Iniciando Projeto:

- npm

```sh
npm init next-app
```

- Yarn

```sh
yarn create next-app
```

### Clonar Projeto

```sh
git clone https://gitlab.com/BDAg/skill.us/skillus-web.git
```

### Rodar Projeto

- npm

```sh
npm dev
```

- Yarn

```sh
yarn dev

```
